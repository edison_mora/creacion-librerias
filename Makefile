# Este es el archivo Makefile de la práctica, editarlo como sea neceario
CC = gcc
CFLAGS=-I./include -lm

# Target que se ejecutará por defecto con el comando make
all: dynamic

# Target que compilará el proyecto usando librerías estáticas
static: ./lib/vector.c ./include/vector.h
	$(CC) -c ./lib/vector.c $(CFLAGS)
	ar rcs ./lib/libvector.a vector.o
	$(CC) -c ./src/main.c $(CFLAGS)
	$(CC) -static -o tareaStatic ./main.o ./lib/libvector.a -lm




# Target que compilará el proyecto usando librerías dinámicas
dynamic: ./lib/vector.c ./include/vector.h
	$(CC) -shared -fPIC -o ./lib/libvector.so ./lib/vector.c $(CFLAGS)
	$(CC) -o tareaDynamic ./src/main.c ./lib/libvector.so $(CFLAGS)

# Limpiar archivos temporales
clean:
	rm -f tarea*
	rm -f *.o