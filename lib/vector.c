/* implementar aquí las funciones requeridas */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector.h>

float dotproduct(vector3D a, vector3D b){
	float result = 0.0;
	result = (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
	return result;
}

float magnitud(vector3D a){
	float magnitud = 0.0;
	magnitud = sqrt( pow(a.x, 2) + pow(a.y, 2) + pow(a.z, 3) );
	return magnitud;
}

vector3D crossProduct( vector3D punto1 , vector3D punto2){
	float coordenadaI , coordenadaY , coordenadaZ;
	vector3D resultado;
	coordenadaI = (punto2.y * punto2.z) - (punto1.z * punto2.y);
	coordenadaY = -((punto1.x * punto2.z) - (punto1.z * punto2.x) );
	coordenadaZ = (punto1.x * punto2.y) - (punto1.y * punto2.x);
	resultado.x = coordenadaI;
	resultado.y = coordenadaY;
	resultado.z = coordenadaZ;
	return resultado;
}

int isOrtogonal( vector3D punto1 , vector3D punto2){
	float productoPunto;
	productoPunto = dotproduct(punto1,punto2);
	if (productoPunto == 0){
		return 1;
	}else{
		return 0;
	}
}