/* Definicion del tipo de datos vector3D */
typedef struct {
	float x;
	float y;
	float z;
} vector3D;

float dotproduct(vector3D a, vector3D b);
vector3D crossProduct( vector3D punto1 , vector3D punto2);
int isOrtogonal( vector3D punto1 , vector3D punto2);
float magnitud(vector3D a);