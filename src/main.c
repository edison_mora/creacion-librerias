#include <stdio.h>
#include <vector.h>

int main(){
	vector3D v1, v2, xprod;
	float productoPunto, magnitudV1, magnitudV2;
	int esOrtogonal;
	printf("Ingrese la coordenada x del primer vector\n");
	scanf("%f", &v1.x);
	printf("\nIngrese la coordenada y del primer vector\n");
	scanf("%f", &v1.y);
	printf("\nIngrese la coordenada z del primer vector\n");
	scanf("%f", &v1.z);

	printf("\nIngrese la coordenada x del segundo vector\n");
	scanf("%f", &v2.x);
	printf("\nIngrese la coordenada y del segundo vector\n");
	scanf("%f", &v2.y);
	printf("\nIngrese la coordenada z del segundo vector\n");
	scanf("%f", &v2.z);

	productoPunto = dotproduct(v1, v2);
	printf("\nLa distancia entre los dos puntos es: %f \n", productoPunto);

	magnitudV1 = magnitud(v1);
	printf("\nLa magnitud del vector 1 es: %f \n", magnitudV1);

	magnitudV2 = magnitud(v2);
	printf("\nLa magnitud del vector 2 es: %f \n", magnitudV2);

	xprod = crossProduct(v1, v2);
	printf("\nEl producto cruz es: [%f, %f, %f]", xprod.x, xprod.y, xprod.z);

	esOrtogonal = isOrtogonal(v1, v2);
	if(esOrtogonal == 1){
		printf("\nEs ortogonal.");
	}else if(esOrtogonal == 0){
		printf("\nNo es ortogonal.");
	}

}